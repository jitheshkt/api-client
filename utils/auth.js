import Router from 'vue-router'

export{isLoggedIn, doLogOut, getHeader};

function isLoggedIn() {
  if(localStorage.AUTH_TOKEN) {
    return true;
  } else {
    return false;
  }
}

function doLogOut() {
  localStorage.removeItem('AUTH_TOKEN');
  localStorage.removeItem('AUTH_USER');
  return true;
}

function getHeader() {
  const header  = {'Authorization': JSON.parse(localStorage.AUTH_TOKEN)};
  return header;
}
