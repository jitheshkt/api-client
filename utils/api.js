import axios from 'axios'
import {getHeader} from './auth'

const BASE_URL  = 'http://192.168.0.61:3000/api/v2';

export {doLogin, allDevices, getUserInfo, getSwitches, updateSwitch, updateDeviceName, updateSwitchName, getDevice}

function doLogin(data) {
  const url   = BASE_URL+'/authenticate';
  return axios.post(url,data);
}

function getDevice(deviceId) {
  const url = BASE_URL+'/devices/'+deviceId;
  return axios.get(url, {headers: getHeader()});
}

function allDevices() {
  const url   = BASE_URL+'/devices';
  return axios.get(url, {headers: getHeader()});
}

function getUserInfo() {
  const url   = BASE_URL+'/user';
  return axios.get(url, {headers: getHeader()});
}

function getSwitches(devicekey) {
  const url   = BASE_URL+'/devices/'+devicekey+'/switches';
  return axios.get(url, {headers: getHeader()});
}

function updateSwitch(payload) {

  const url   = BASE_URL+'/devices/'+payload.devicekey+'/'+payload.switch_code+'/status';
  var params = {'status' : payload.status};
  return axios.post(url, params, {headers: getHeader()});
}

function updateDeviceName(payload) {
  const url   = BASE_URL+'/devices/'+payload.id+'/meta';
  var params = {'name' : payload.name};
  if(payload.id) {
    return axios.post(url, params, {headers: getHeader()});
  }
}

function updateSwitchName(payload) {
  const url   = BASE_URL+'/devices/'+payload.deviceid+'/'+payload.switchid+'/meta';
  var params = {'name' :payload.name};
  if(payload.switchid) {
    return axios.post(url, params, {headers: getHeader()});
  }
}
