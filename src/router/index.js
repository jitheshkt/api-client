import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Devices from '@/components/Devices'
import Device from '@/components/Device'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.environment === 'development' ? '/' : '/project/automation/',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/devices',
      name: 'Devices',
      component: Devices,
      meta: {requireAuth : true}
    },
    {
      path: '/devices/:devicekey',
      name: 'Device',
      component: Device,
      meta: {requireAuth: true}
    },
  ]
});

export default router;

router.beforeEach((to, from, next) => {
  // ...
  if(to.meta.requireAuth) {
    if(localStorage.AUTH_TOKEN) {
      next();
    } else {
      next({name: 'Login'});
    }
  } else {
    next();
  }
})
